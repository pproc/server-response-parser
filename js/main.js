document.getElementById("decodeButton").addEventListener("click", function() {
    var parameters = document.getElementById("params-input").value,
        jsonParameters = parser.getJsonFromParams(parameters),
        parametersObject = parser.splitComplexKeys(jsonParameters),
        resultElement = document.getElementById("params-output");

    resultElement.innerHTML = parser.syntaxHighlight(parametersObject);
    fileDownloader.download(JSON.stringify(parametersObject), "response.json");
});