var typeChecker = {
    getClassNameBasedOnType: function(text){
        if (typeChecker.isItKey(text)) {
            return 'key';
        }
        else if(typeChecker.isItString(text)){
            return'string';
        }
        else if (typeChecker.isItBoolean(text)) {
            return 'boolean';
        }
        else if (typeChecker.isItNull(text)) {
            return 'null';
        }
        else{
            return 'number';
        }
    },

    isItString: function(text){
        return this._doesItStartWithQuoteMark(text) && !this._isItKeyType(text);
    },

    isItKey: function(text){
        return this._doesItStartWithQuoteMark(text) && this._isItKeyType(text);
    },

    _doesItStartWithQuoteMark: function(text) {
        return /^"/.test(text);
    },

    _isItKeyType: function(text){
        return /:$/.test(text);
    },

    isItBoolean: function(text){
        return /true|false/.test(text);
    },

    isItNull: function(text){
        return /null/.test(text);
    },

    isItStringType: function(variable){
        return typeof variable === 'string';
    }
};