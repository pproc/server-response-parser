var parser = {

    AND_MARK: "&",
    VALUE_MARK: "=",
    DOT_MARK: ".",

    EXTRACT_ALL_PHRASES_REGEX: /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
    REPLACE_AMPERSAND: [/&/g, "&amp;"],
    REPLACE_LESS_THAN: [/</g, "&lt;"],
    REPLACE_GREATER_THAN: [/>/g, "&gt;"],

    getJsonFromParams: function(params) {
        var me = this,
            result = {},
            item;

        params.split(me.AND_MARK).forEach(function(part) {
            item = part.split(me.VALUE_MARK);

            result[item[0]] = decodeURIComponent(item[1]);
        });

        return result;
    },

    splitComplexKeys: function(json) {
        var me = this,
            result = {};

        Object.keys(json).forEach(function(parameter) {
            var keys = parameter.split(me.DOT_MARK);
            if (keys.length > 1) {
                if (!result[keys[0]]) {
                    result[keys[0]] = {}
                }

                result[keys[0]][keys.slice().splice(1).join(me.DOT_MARK)] = json[parameter];

                if (keys.length > 2) {
                    result[keys[0]] = me.splitComplexKeys(result[keys[0]]);
                }
            }
            else {
                result[parameter] = json[parameter];
            }
        });

        return result;
    },

    syntaxHighlight: function(json) {
        var me = this;

        if (!typeChecker.isItStringType(json)) {
            json = JSON.stringify(json, undefined, 2);
        }

        json = me.replaceSpecialCharacters(json);

        return json.replace(me.EXTRACT_ALL_PHRASES_REGEX, me.addFormattingToPhrase.bind(me));
    },

    replaceSpecialCharacters: function(text) {
        text = text.replace.apply(text, this.REPLACE_AMPERSAND);
        text = text.replace.apply(text, this.REPLACE_LESS_THAN);
        text = text.replace.apply(text, this.REPLACE_GREATER_THAN);

        return text;
    },

    addFormattingToPhrase: function(match) {
        var className = typeChecker.getClassNameBasedOnType(match);

        return this._createSpan(match, className);
    },

    _createSpan: function(text, className) {
        return '<span class="' + className + '">' + text + '</span>';
    }
};